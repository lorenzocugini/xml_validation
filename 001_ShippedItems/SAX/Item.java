/**
 * Class to represent one item batch of a shipment.
 */
public class Item
{
    private String title;
    private String note;
    private int quantity;
    private float price;

    /**
     * Default constructor
     */
    public Item() {}

    public void setTitle(String title) {
        this.title = title;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    String to_string()
    {
        return new String( "Title:\t" + title + "\nNote:\t" + note + "\nQuantity:\t" + quantity + "\nPrice:\t" + price );
    }
}
