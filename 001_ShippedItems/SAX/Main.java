public class Main
{
    static final String filename = "path_to_file";

    public static void main (String[] args)
    {
        Parser p = new Parser(filename);

        try
        {
            p.parse();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        System.out.println(p.shiporder.to_string());
    }
}
