import java.util.ArrayList;

/**
 * Class for the whole ship order.
 */
public class Shiporder
{
    private int orderid;
    private String orderperson;
    private Shipto shipto;
    private ArrayList<Item> items;

    /**
     * Default constructor
     */
    public Shiporder()
    {
        items = new ArrayList<Item>();
    }

    /**
     * Add an item to items' arraylist.
     */
    public void addItem(Item item)
    {
        this.items.add(item);
    }

    public void setOrderid(int orderid) {
        this.orderid = orderid;
    }

    public void setOrderperson(String orderperson) {
        this.orderperson = orderperson;
    }

    public void setShipto(Shipto shipto) {
        this.shipto = shipto;
    }

    String to_string()
    {
        String tmp = "";

        tmp += "\tOrder: " + orderid + "\n";
        tmp += orderperson + "\n";
        tmp += shipto.to_string() + "\n\n";

        for(Item it : items)
        {
            tmp += it.to_string() + "\n\n";
        }

        return tmp;
    }
}