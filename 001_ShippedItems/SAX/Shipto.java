/**
 * Class to represent the recipient of a shipment.
 */
public class Shipto
{
    private String name;
    private String address;
    private String city;
    private String country;

    /**
     * Default constructor
     */
    public Shipto() {}

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    String to_string()
    {
        return new String("Name:\t" + name + "\nAddress:\t" + address + "\nCity:\t" + city + "\nCountry:\t" + country);
    }
}