import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

public class Parser
{
    //File to be parsed
    private String filename;

    //Result of parsing is stored here
    public Shiporder shiporder = new Shiporder();

    //Support for uncompletely parsed tags
    protected String tmp_str;
    protected Item tmp_item;
    protected Shipto tmp_shipto;

    /**
     * Default constructor
     */
    public Parser(String filename)
    {
        this.filename = filename;
    }

    /**
     * Method to perform the parsing.
     */
    public void parse() throws Exception
    {
        SAXParserFactory spf = SAXParserFactory.newDefaultInstance();
        SAXParser sap = spf.newSAXParser();

        sap.parse(new File(filename), new Handler());
    }

    /**
     * Inner class for handlers.
     */
    class Handler extends DefaultHandler
    {
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes)
        {
            switch( qName.toLowerCase() )
            {
                case "shipto" :
                    tmp_shipto = new Shipto();
                    break;

                case "item" :
                    tmp_item = new Item();
                    break;

                case "shiporder" :
                    int length = attributes.getLength();

                    for(int i=0; i<length; i++)
                    {
                        //The only attribute of orderperson is orderid -> avoid tedious checking
                        String attrName = attributes.getQName(i);
                        String attrValue = attributes.getValue(i);

                        shiporder.setOrderid( Integer.parseInt(attrValue));
                    }
                    break;
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName)
        {
            switch( qName.toLowerCase() )
            {
                case "name" :
                    tmp_shipto.setName(tmp_str);
                    break;
                case "address" :
                    tmp_shipto.setAddress(tmp_str);
                    break;
                case "city" :
                    tmp_shipto.setCity(tmp_str);
                    break;
                case "country" :
                    tmp_shipto.setCountry(tmp_str);
                    break;

                case "title" :
                    tmp_item.setTitle(tmp_str);
                    break;
                case "note" :
                    tmp_item.setNote(tmp_str);
                    break;
                case "quantity" :
                    tmp_item.setQuantity( Integer.valueOf(tmp_str) );
                    break;
                case "price" :
                    tmp_item.setPrice( Float.parseFloat(tmp_str) );
                    break;

                case "orderperson" :
                    shiporder.setOrderperson(tmp_str);
                    break;
                case "item" :
                    shiporder.addItem(tmp_item);
                    break;
                case "shipto" :
                    shiporder.setShipto(tmp_shipto);
                    break;
            }
        }

        @Override
        public void characters(char[] ch, int start, int length)
        {
            tmp_str = new String(ch, start, length);
        }
    }
}
