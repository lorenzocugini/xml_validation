# KnowledgeContainers

A metadata exercise collection. Among various ways of support a Semantic Web, you can find various representation of the same exercise. Please note that:  
**Data representations** : those are representation of data, single instance of a schema. **[XML, N3, RDF]**  
**Metadata syntax validation** : those are the schemas, independent from a single instance. They can validate the correspondent Data Representation. **[DTD, XSD]**  
**Semantics** : formats to specify the semantics behind a schema **[RDFS, OWL, GrOWL]**  
**Query for semantics** : query languages with semantic support **[SPARQL]**  
**Support** : knowledge handling supports. **[SAX - Library for XML Parsing in Java]**  
**Ontology support** : ontology specified through the use of Protégé - https://protege.stanford.edu/ **[PRO, PRO-Q]**  

Exercises' tags. If a tag appears within [..], then the correspondent syntax is too weak to correctly represent the problem. You may find:  
    **XML** eXtensible Markup Language;  
    **N3** Notation3;  
    **RDF** Resource Description Framework;  
    **DTD** Document Type Definition;  
    **XSD** XML-Schema Definition;  
    **RDFS** Resource Description Framework Schema;  
    **OWL** Web Ontology Language;  
    **GrOWL** Graphical OWL;  
    **SAX** Simple Api for XML Processing - written in Java;  
    **SPARQL** SPARQL Protocol and RDF Query Language;  
    **PRO** Ontology specification with Protégé;  
    **PRO-Q** Some queries specified wrt the ontology;  
           
State of the art:  
001 -   XML     N3      RDF     DTD     XSD     RDFS     OWL    SAX  
002 -   XML     [N3]    [RDF]   DTD     XSD  
003 -   XML     [DTD]   XSD  
004 -   XML     DTD     XSD  
005 -   XML     DTD     XSD  
006 -   XML     N3      RDF     DTD     XSD     SAX    
007 -   XML     N3      RDF  
  
  
Exercises taken from:  
001 - Shipped Items     -   http://www.dia.uniroma3.it/~atzeni/didattica/BD/20112012/Ex%2001%20DTD.pdf    
002 - Letter            -   http://www.dia.uniroma3.it/~atzeni/didattica/BD/20112012/Ex%2001%20DTD.pdf  
003 - Binary Strings    -   http://www.dia.uniroma3.it/~atzeni/didattica/BD/20122013/ExercisesXSD.pdf  
004 - Invoices          -   Computer Science and Engineering PoliMi - Knowledge Engineering 2016–17 Test 5thFebruary 2018  
005 - Mountains         -   http://torlone.dia.uniroma3.it/bd2/BD2-ES1.pdf  
006 - Plant Catalogue   -   https://www.w3schools.com/xml/plant_catalog.xml (with custom modifications)    
007 - CD Catalogue      -   http://hp1.gcal.ac.uk/help/wvtxmlex.htm  