import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

public class Parser
{
    private String filename;

    //Actual catalog
    public Catalog catalog;

    //Support variables
    private Plant tmp_plant;
    private String tmp_str;

    /**
     * Default constructor
     */
    public Parser(String filename)
    {
        this.filename = filename;
        this.catalog = new Catalog();
    }

    /**
     * Method to perform the parsing.
     */
    public void parse() throws Exception
    {
        SAXParserFactory spf = SAXParserFactory.newDefaultInstance();
        SAXParser sap = spf.newSAXParser();

        sap.parse(new File(filename), new Handler());
    }


    //Inner class for parsing
    class Handler extends DefaultHandler
    {
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes)
        {
            switch( qName.toLowerCase() )
            {
                case "plant" :
                    tmp_plant = new Plant();
                    break;
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName)
        {
            switch( qName.toLowerCase() )
            {
                case "common" :
                    tmp_plant.setCommon(tmp_str);
                    break;
                case "botanical" :
                    tmp_plant.setBotanical(tmp_str);
                    break;
                case "zone" :
                    tmp_plant.setZone(Integer.parseInt(tmp_str));
                    break;
                case "light" :
                    tmp_plant.setLight(tmp_str);
                    break;
                case "price" :
                    tmp_plant.setPrice(tmp_str);
                    break;
                case "availability" :
                    tmp_plant.setAvailability(Long.parseLong(tmp_str));
                    break;

                case "plant" :
                    catalog.addPlant(tmp_plant);
                    break;
            }
        }

        @Override
        public void characters(char[] ch, int start, int length)
        {
            tmp_str = new String(ch, start, length);
        }
    }
}
