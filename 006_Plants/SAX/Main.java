public class Main
{
    static final String filename = "/home/lorenzo/Documents/XML/plants.xml";

    public static void main (String[] args)
    {
        Parser p = new Parser(filename);

        try
        {
            p.parse();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        System.out.println(p.catalog.to_string());
    }
}
