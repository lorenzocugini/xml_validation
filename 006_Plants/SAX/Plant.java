public class Plant
{
    private String common;
    private String botanical;
    private int zone;
    private String light;
    private String price;
    private long availability;

    public Plant() {}

    public String to_string()
    {
        return new String("Common:\t" + common + "\nBotanical:\t" + botanical + "\nZone:\t" + zone + "\nLight:\t" + light + "\nPrice:\t" + price + "\nAvailability:\t" + availability + "\n" );
    }

    public void setCommon(String common) {
        this.common = common;
    }

    public void setBotanical(String botanical) {
        this.botanical = botanical;
    }

    public void setZone(int zone) {
        this.zone = zone;
    }

    public void setLight(String light) {
        this.light = light;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setAvailability(long availability) {
        this.availability = availability;
    }
}