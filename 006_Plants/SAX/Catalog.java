import java.util.ArrayList;

public class Catalog
{
    ArrayList<Plant> catalog;

    public Catalog()
    {
        catalog = new ArrayList<Plant>();
    }

    public void addPlant(Plant p)
    {
        this.catalog.add(p);
    }

    public String to_string()
    {
        String tmp = "";

        for(Plant p : catalog)
        {
            tmp += p.to_string();
        }

        return tmp;
    }
}